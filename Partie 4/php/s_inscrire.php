
<?php
session_start(); 
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/code1.css" />
        <title>Projet_Offoue</title>
    </head>
    <body>
        <div id="div1">
        <?php include("header.php") ;?>
            <div id="div4">
            <?php  include("section.php"); ?>
                <aside>
                    <h1><strong>s'inscrire</strong></h1>
                    <form method="POST" id="Envoyer" action="bdd.php">
                        <p>
                            <label for="nom">Nom :</label>
                            <input type="text" id="nom" name="nom" required />
                            <span id="nom_manquant"></span>
                            
                             
                       </p>
                       <p>
                            <label for="prenom">Prenom :</label>
                            <input type="text" id="prenom" name="prenom"  required />
                            <span id="prenom_manquant"></span>
                            
                            
                       </p>
                       <p>
                           <label for="jour">Date de naissance :</label>
                           <input type="date" id="jour" name="jour" required />
                           <span id="jour_manquant"></span>
                           
                       </p>
                       <p>
                       <label for="genre">genre :</label>
                         <select name="genre" id="genre" >
                              <option value="Feminin">Feminin</option>
                              <option value="Masculin">Masculin</option>
                              <option value="Autre">Autre</option>
                         </Select>
                       </p>
                       <p>
                           <label for="mail">E-mail :</label>
                           <input placeholder="ex : ouffoue@exemple.com" type="email" id="mail" name="mail" required />
                           <span id="mail_manquant"></span>
                          
                       </p>
                       <p>
                           <label for="mdp">mot de passe :</label>
                           <input placeholder="An@lyse42" type="password" id="mdp" name="mdp" required />
                           <span id="mot_de_passe_manquant"></span>
                          
                       </p>
                        <p>
                           <label for="tel">téléphonne :</label>
                           <input type="tel" id="tel" name="tel" required />
                           <span id="tel_manquant"></span>
                           
                       </p>
                       <p>
                   
                           <label for="codepost">code postal :</label>
                           <input type="tel" id="codepost" name="codepost" maxlength="5" required />
                           <span id="codepost_manquant"></span>
                           
                       </p>
                        <p>
                   
                           <label for="Adresse">Adresse :</label>
                           <input type="text" id="Adresse" name="adresse" required />
                           <span id="Adresse_manquant"></span>
                           
                        </p>
                        <p>
                      
                           <label for="Ville">Ville :</label>
                           <input type="text" id="Ville" name="ville" required />
                           <span id="Ville_manquant"></span>  
                          
                       </p>
                       <P>
                      
                           <input type="reset" value="Remettre à zéros" />
                           <input type="submit" value="Envoyer" onclick="validate()" />
                   
                       </P>
                    </form>
                    <p style="color: red ;" id="erreur"> <strong></strong></p>
                    

                </aside>
            </div>
          
            <footer>
                <p>Copyright Onack et Ghislain</p>
                <p> CY-TECH web</p>
            </footer>
        </div>
        <script src="javaScript/codeJava4.js"></script>
        <html>
   <head>
      <script>
         function checkEmail(mail) {
             var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
             return re.test(mail);
         }
         function validate() {
             var mail = document.getElementById("mail").value;
         
             if (checkEmail(mail)) {
                 alert('Adresse e-mail valide');
             } else {
                 alert('Adresse e-mail non valide');
             }
             return false;
         }
      </script>
   </head>
   <body>
      <p>Entrez une adresse email:</p>
      <input id='email'>
      <button type='submit' onclick="validate()">Validate!</button>
   </body>
</html>
    </body>
</html>