<?php 

// connection à la base de donnée
$IDENTIFIANT='root';
$MOT_DE_PASSE='';
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=restaurant_ivoirien;charset=UTF8', $IDENTIFIANT, $MOT_DE_PASSE);

}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}

// on fait des requetes pour ajouter les donnéés du formulaire de contact 

 $nom = $_POST['nom'];
 $prenom = $_POST['prenom'];
 $jour = $_POST['jour'];
 $mail = $_POST['mail'];
 $mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT) ; // on ajoute une fonction de hachage du mdp 
 $genre = $_POST['genre'];
 $tel = $_POST['tel'];
 $codepost = $_POST['codepost'];
 $adresse = $_POST['adresse'];
 $ville = $_POST['ville'];

$req = $bdd->prepare('INSERT INTO client(nom_client,	prenom_client,	date_de_naissance_client,	genre_client	,email_client,  mot_de_passe, telephone_client,	code_postal,	adresse_client,	ville) 
VALUES(:nom_client,	:prenom_client,	:date_de_naissance_client,	:genre_client,	:email_client,  :mot_de_passe,	:telephone_client,	:code_postal,	:adresse_client	, :ville)' );

$req->execute(array(
	'nom_client' =>$nom,
    'prenom_client'=>$prenom,
    'date_de_naissance_client'=>$jour,
    'genre_client' =>$genre,
    'email_client' =>$mail,
    'mot_de_passe' =>$mdp,
    'telephone_client' =>$tel,
    'code_postal' =>$codepost,
    'adresse_client' =>$adresse,
    'ville' =>$ville

));

include("s'inscrire.php");


?>