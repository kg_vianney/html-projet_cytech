<?PHP

$_SESSION['categories'] = [

    // modif 4/13/2021
    [
        
        'name' => 'Accueil',
        'route' => 'accueil',
    
    ],

    [
        'name' => 'Grillades',
        'route' => 'grillade',
        'reference' => ['g1', 'g2', 'g3', 'g4'],
        'designation' => ['Alloco', 'shoukouya', 'poulet_braisé', 'Porc_au_four'],
        'photo' => ['Alloco Image - Copy.jpg', 'Choucouya mouton images - Copy.jfif', 'Poulet braisé Image.jfif', 'poisson-braisé-grillé-ivorianfood - Copy.png'],
        'prix' => ['100£', '200£', '150£', '200£'],
        'stock' => ['200', '200', '200', '200','200']
    ],
    [
        'name' => 'Accompagnement',
        'route' => 'accompagnement',
        'reference' => ['A1', 'A2', 'A3', 'A4', 'A5'],
        'designation' => ['Foutou_banane', 'Foutou_igname', 'Placali', 'Riz', 'Attiéké'],
        'photo' => ['Plat_de_foufou_huile_rouge - Copy.jfif', 'Foutou igname Image - Copy.jpg', 'Placali Im.jfif', 'Riz Image - Copy.jpg', 'Attiéké Image - Copy.jpg'],
        'prix' => ['100£', '200£', '150£', '200£','200£'],
        'stock' => ['200', '200', '200', '200','200']
    ],
    [
        'name' => 'Soupes et Sauces',
        'route' => 'soupe_et_sauces',
        'reference' => ['S1', 'S2', 'S3', 'S4', 'S5'],
        'designation' => ['sauce_Aubergine', 'Sauce_Graine', 'Sauce_Arachide', 'Sauce_Côpê', 'Soupe_de_Poisson'],
        'photo' => ['sauce aubergine.jpg', 'sauce-graine-Image.jpg', 'sauce arachide Image - Copy.jpg', 'Sauce côpê image.jpg', 'Sauce claire + poisson images - Copy.jfif'],
        'prix' => ['100£', '200£', '150£', '200£','200£'],
        'stock' => ['200', '200', '200', '200','200']
    ],
    [
        'name' => 'Jus et Boissons',
        'route' => 'jus_et_boissons',
        'reference' => ['B1', 'B2', 'B3', 'B4', 'B5'],
        'designation' => ['Bissap', 'Gnamakou', 'Tomy', 'Dêguê', 'Passion'],
        'photo' => ['bissap image - Copy - Copy.jpg', 'gnamankoudji - Copy.jpg', 'jus de tomy.jpg', 'Dêguê - Copy.jfif', 'Jus de passion.jpg'],
        'prix' => ['100£', '200£', '150£', '200£','200£'],
        'stock' => ['200', '200', '200', '200','200']
    ],

];


$_SESSION['grillade'] = array ('reference', 'designation', 'photos', 'prix','stock','commande');
$_SESSION['reference'] = array ('g1', 'g2', 'g3', 'g4');
$_SESSION['designation'] = array ('Alloco', 'shoukouya', 'poulet_braisé', 'Porc_au_four');
$_SESSION['photo'] = array ('', '', '', '');
$_SESSION['prix'] = array ('100£', '200£', '150£', '200£','200£');
$_SESSION['stock'] = array ('200', '200', '200', '200','200');


?>