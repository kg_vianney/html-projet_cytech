#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Produits
#------------------------------------------------------------

CREATE TABLE Produits(
        id_produit        Int  Auto_increment  NOT NULL ,
        nom_produit       Varchar (50) NOT NULL ,
        categorie_produit Varchar (50) NOT NULL ,
        prix_produit      Float NOT NULL ,
        stock_produit     Int NOT NULL ,
        reference_produit Varchar (50) NOT NULL
	,CONSTRAINT Produits_PK PRIMARY KEY (id_produit)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Client
#------------------------------------------------------------

CREATE TABLE Client(
        id                       Int Auto_increment  NOT NULL  ,
        nom_client               Varchar (50) NOT NULL ,
        prenom_client            Varchar (50) NOT NULL ,
        date_de_naissance_client Date NOT NULL ,
        genre_client             Char (50) NOT NULL ,
        email_client             Char (50) NOT NULL ,
        telephone_client         Int NOT NULL ,
        code_postal              Int NOT NULL ,
        adresse_client           Char (50) NOT NULL ,
        Ville                    Char (50) NOT NULL
	,CONSTRAINT Client_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commande
#------------------------------------------------------------

CREATE TABLE Commande(
        id_commande   Int  Auto_increment  NOT NULL ,
        date_commande Date NOT NULL ,
        id            Int NOT NULL
	,CONSTRAINT Commande_PK PRIMARY KEY (id_commande)

	,CONSTRAINT Commande_Client_FK FOREIGN KEY (id) REFERENCES Client(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: contient
#------------------------------------------------------------

CREATE TABLE contient(
        id_produit  Int NOT NULL ,
        id_commande Int NOT NULL ,
        qte_produit Int NOT NULL
	,CONSTRAINT contient_PK PRIMARY KEY (id_produit,id_commande)

	,CONSTRAINT contient_Produits_FK FOREIGN KEY (id_produit) REFERENCES Produits(id_produit)
	,CONSTRAINT contient_Commande0_FK FOREIGN KEY (id_commande) REFERENCES Commande(id_commande)
)ENGINE=InnoDB;

