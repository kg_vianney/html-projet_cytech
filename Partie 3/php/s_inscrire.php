
<?php
session_start(); 
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/code1.css" />
        <title>Projet_Offoue</title>
    </head>
    <body>
        <div id="div1">
        <?php include("header.php") ;?>
            <div id="div4">
            <?php  include("section.php"); ?>
                <aside>
                    <h1><strong>s'inscrire</strong></h1>
                    <form method="POST" id="Envoyer" action="verifInscription.php">
                        <p>
                            <label for="nom">Nom :</label>
                            <input type="text" id="nom" name="nom" />
                            <span id="nom_manquant"></span>
                            
                             
                       </p>
                       <p>
                            <label for="prenom">Prenom :</label>
                            <input type="text" id="prenom" name="prenom" />
                            <span id="prenom_manquant"></span>
                            
                            
                       </p>
                       <p>
                           <label for="jour">Date de naissance :</label>
                           <input type="date" id="jour" name="jour" />
                           <span id="jour_manquant"></span>
                           
                       </p>
                       <p>
                       <label for="genre">genre :</label>
                         <select name="genre" id="genre" >
                              <option value="Feminin.html">Feminin</option>
                              <option value="Masculin.html">Masculin</option>
                              <option value="Autre.html">Autre</option>
                         </Select>
                       </p>
                       <p>
                           <label for="mail">E-mail :</label>
                           <input placeholder="ex : ouffoue@exemple.com" type="email" id="mail" name="mail" />
                           <span id="mail_manquant"></span>
                          
                       </p>
                       <p>
                           <label for="tel">téléphonne :</label>
                           <input type="tel" id="tel" name="tel" />
                           <span id="tel_manquant"></span>
                           
                       </p>
                       <p>
                   
                           <label for="codepost">code postal :</label>
                           <input type="tel" id="codepost" name="codepost" maxlength="5" />
                           <span id="codepost_manquant"></span>
                           
                       </p>
                        <p>
                   
                           <label for="Adresse">Adresse :</label>
                           <input type="text" id="Adresse" name="Adresse" />
                           <span id="Adresse_manquant"></span>
                           
                        </p>
                        <p>
                      
                           <label for="Ville">Ville :</label>
                           <input type="text" id="Ville" name="Ville" />
                           <span id="Ville_manquant"></span>  
                          
                       </p>
                       <P>
                      
                           <input type="reset" value="Remettre à zéros" />
                           <input type="submit" value="Envoyer" />
                   
                       </P>
                    </form>
                    <p style="color: red ;" id="erreur"> <strong></strong></p>
                    

                </aside>
            </div>
          
            <footer>
                <p>Copyright Onack et Ghislain</p>
                <p> CY-TECH web</p>
            </footer>
        </div>
        <script src="javaScript/codeJava4.js"></script>
    </body>
</html>